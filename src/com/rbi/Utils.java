package com.rbi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.stream.Stream;

/**
 * Created by ron on 14/05/16.
 */
public class Utils {

    public static Stream<String> systemStream(){
        return new BufferedReader(new InputStreamReader(System.in)).lines();
    }

}
