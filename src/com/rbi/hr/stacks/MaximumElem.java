package com.rbi.hr.stacks;

import java.util.ArrayDeque;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.function.BiFunction;
import java.util.stream.IntStream;

public class MaximumElem {

    TrackedStack<Integer> stack;

    MaximumElem(){
        stack = new TrackedStack<>( (cur, elem) -> elem.compareTo(cur) >0 );
    }

    /**
     * 1 x  -Push the element x into the stack.
     * 2    -Delete the element present at the top of the stack.
     * 3    -Print the maximum element in the stack.
     * @param input - scanner instance
     */
    void query(Scanner input){
        switch(input.nextInt()){
            case 1: stack.push( input.nextInt() ); break;
            case 2: stack.pop(); break;
            case 3: System.out.println(stack.getTracked()); break;
        }
    }

    public static void main(String[] args) {
        MaximumElem instance = new MaximumElem();
        Scanner scanner = new Scanner(System.in);
        IntStream.range(0, scanner.nextInt())
                .forEach( i -> instance.query(scanner) );
    }

    class TrackedStack<E>{
        final ArrayDeque<E> stack;
        final ArrayDeque<Count<E>> tracked;
        final BiFunction<E, E, Boolean> trackDelegate;

        public TrackedStack(BiFunction<E, E, Boolean> trackDelegate) {
            this.stack = new ArrayDeque<>();
            this.tracked = new ArrayDeque<>();
            this.trackDelegate = trackDelegate;
        }

        E getTracked(){
            Count<E> track = tracked.peekLast();
            return (track ==null) ? null : track.value;
        }

        void push(E elem){
            Count<E> track = tracked.peekLast();
            // push new track if first push or instructed to by delegate
            if(track==null || trackDelegate.apply(track.value, elem))
                tracked.addLast(new Count(elem));
            // increment trakc otherwise
            else track.inc();
            // push elem to stack
            stack.addLast(elem);
        }

        E pop(){
            Count<E> track = tracked.peekLast();
            // if tracked empty, then stack is empty and can't pop
            if(track==null) throw new NoSuchElementException();
            // if current track is invalid then pop
            if(track.dec()) tracked.removeLast();
            // pop stack elem
            return stack.removeLast();
        }
    }

    /**
     * Container class to store a value and a count
     */
    class Count<E>{
        E value;
        int count;
        public Count(E value) {
            this.value = value;
            count=0;
        }
        /**
         * increments count
         */
        void inc(){++count;}
        /**
         * @return true if count is sub-zero after decrement, false otherwise.
         */
        boolean dec(){return (--count <0);}
    }
}
