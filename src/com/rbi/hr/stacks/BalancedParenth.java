package com.rbi.hr.stacks;

import java.util.ArrayDeque;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by ron on 15/05/16.
 */
public class BalancedParenth {

    static ArrayDeque<Bracket> stack;
    static{
        stack = new ArrayDeque<>();
    }

    public static void processSequence(String sequence){
        // use stack to match left and right
        for (int i = 0; i < sequence.length(); ++i) {
            Bracket b = Bracket.of(sequence.charAt(i));
            if(b.isRight()){
                if(stack.isEmpty() || !stack.removeLast().matches(b)){
                    System.out.println("NO");
                    return;
                }
            }
            // add to stack if left
            else stack.addLast(b);
        }
        // make sure stack is empty
        System.out.println((stack.isEmpty())? "YES" : "NO");
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        IntStream.range(0, scanner.nextInt())
                // clear stack for next sequence
                .peek( i -> stack.clear() )
                .forEach( i -> processSequence(scanner.next()) );
    }

    enum Bracket{
        //l=left r=right
        //c=braCes, k=bracKets, p=Parenthesis
        lc,lk,lp,rc,rk,rp;

        /**
         * @return true if right side, false otherwise
         */
        boolean isRight(){
            switch(this){
                case lc:case lk:case lp: return false;
                default: return true;
            }
        }

        boolean matches(char c){
            return matches(of(c));
        }

        boolean matches(Bracket b){
            switch(this){
                case lc: return b.equals(rc);
                case lk: return b.equals(rk);
                case lp: return b.equals(rp);
                case rc: return b.equals(lc);
                case rk: return b.equals(lk);
                case rp: return b.equals(lp);
                default: return false;
            }
        }

        /**
         * @return Bracket enum for char 'c'
         */
        static Bracket of(char c){
            switch(c){
                case '{':return lc;
                case '[':return lk;
                case '(':return lp;
                case '}':return rc;
                case ']':return rk;
                case ')':return rp;
                default: throw new RuntimeException("unsupported bracket type: "+c);
            }
        }
    }
}
