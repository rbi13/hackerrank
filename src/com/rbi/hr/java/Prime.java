package com.rbi.hr.java;


/**
 * Created by ron on 15/05/16.
 */

// needed for 'gotchya' in question

class Prime{

    void checkPrime(int ... n){
        for(int num : n){
            if(num<2) continue;
            boolean print=(true);
            int limit = (int) Math.sqrt(num);
            for(int i=2; i<=limit; ++i){
                if(num%i ==0){
                    print=false; break;
                }
            }
            if(print) System.out.print(num+" ");
        }
        System.out.println();
    }
}
