package com.rbi.hr.arrays;

import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class SparseArrays {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, Long> counts = IntStream.range(0, scanner.nextInt())
                .mapToObj( i -> scanner.next() )
                .collect(groupingBy(identity(), counting()));

        IntStream.range(0, scanner.nextInt())
                .mapToLong( i -> counts.getOrDefault(scanner.next(), 0l) )
                .forEach( System.out::println );
    }
}
